﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedDemonAI : MonoBehaviour
{
    public enum Character {
        AGGRESSIVE,
        PASSIVE
    }

    public enum State {
        IDLE,
        MOVING_IN,
        ATTACKING,
        FLEEING,
        HIT,
        DYING
    }

    public Character _character;
    
    public State _currentstate;

    [SerializeField]
    private GameObject _projectile;

    [SerializeField]
    private Transform _targetPlayer;

    [SerializeField, Range( 0, 50 )]
    private int _projectileDamage = 7;

    [SerializeField, Range(0, 30)]
    private float _movementSpeed = 8, _fleeingSpeed = 4, _knockbackSpeed = 30, _projectileSpeed = 3;

    [SerializeField, Range( 0, 100 )]
    private float _detectRange = 100, _attackRange = 12, _fleeRange = 6;

    [SerializeField, Range( 0, 3 )]
    private float _cooldown = 0.9f;

    [SerializeField, Range(0, 2)]
    private float _hitTime = 0.8f, _deathTime = 1.2f;

    private Vector3 _targetLocation;

    private float _projectileOffset = 0.9f;
    private float _targetOffset = 1.75f;
    private float _currentCooldown = 0;

    private float _currentHitTime;
    private float _currentDeathTime;
    

    private void Update() {
        // TODO: if paused, return

        if ( _currentstate != State.DYING )
        {

            if ( _currentCooldown > 0 )
                _currentCooldown -= Time.deltaTime;

            _targetPlayer = TryGetClosestPlayer();
            if ( null == _targetPlayer )
                return;

            if ( _currentstate != State.HIT )
            {
                float distance = Vector3.Distance(transform.position, _targetPlayer.position);


                if ( _character == Character.PASSIVE && distance <= _fleeRange )
                {
                    _currentstate = State.FLEEING;
                }
                else if ( distance <= _attackRange )
                    _currentstate = State.ATTACKING;
                else
                    _currentstate = State.MOVING_IN;


                switch ( _currentstate )
                {
                    case State.MOVING_IN:
                        Move();
                        break;
                    case State.ATTACKING:
                        if ( _currentCooldown <= 0 )
                        {
                            Debug.Assert(null != _projectile, "Projectile instance missing!");
                            GameObject projectile = Instantiate(_projectile, transform.position + transform.forward * _projectileOffset, Quaternion.identity);

                            Vector3 targetPos = new Vector3(_targetPlayer.position.x, transform.localScale.y / 2, _targetPlayer.position.z);
                            projectile.transform.LookAt(targetPos);

                            Projectile projectileScript = projectile.GetComponent<Projectile>();
                            projectileScript.Speed = _projectileSpeed;
                            projectileScript.Damage = _projectileDamage;

                            _currentCooldown = _cooldown;
                        }
                        break;
                    case State.FLEEING:
                        Flee();
                        break;
                }
            }
            else {
                IEnumerator coroutine = HitTimer( _currentHitTime );
                StartCoroutine(coroutine);
            }
        }
        else {
            IEnumerator coroutine = DeathTimer( _currentDeathTime );
            StartCoroutine(coroutine);
        }
    }

    private Transform TryGetClosestPlayer() {
        Transform target = null;
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        Debug.Assert( players.Length > 0, "No Players could be detected." );

        float currentHighest = Mathf.Infinity;
        for ( int i = 0; i < players.Length; i++ ) {
            float distance = Vector3.Distance( transform.position, players[ i ].transform.position );

            if ( distance <= _detectRange && distance <= currentHighest ) {
                target = players[ i ].transform;
                currentHighest = distance;
            }
        }

        return target;
    }

    public void Hit() {
        if ( _currentstate != State.HIT ) {
            _currentstate = State.HIT;
            _currentHitTime = _hitTime;
            IEnumerator coroutine = HitTimer( _currentHitTime );
            StartCoroutine(coroutine);
        }
    }

    public void Death() {
        if ( _currentstate != State.DYING ) {
            _currentstate = State.DYING;
            _currentDeathTime = _deathTime;
            IEnumerator coroutine = DeathTimer( _currentDeathTime );
            StartCoroutine( coroutine );
        }
    }

    private void Move() {
        _targetLocation = new Vector3( _targetPlayer.position.x, transform.localScale.y / 2, _targetPlayer.position.z ) + _targetPlayer.forward * _targetOffset;

        transform.position = Vector3.MoveTowards( transform.position, _targetLocation, _movementSpeed / 100 );
    }

    private void Flee() {
        _targetLocation = new Vector3( transform.position.x, transform.localScale.y / 2, transform.position.z ) + Vector3.forward;

        transform.position = Vector3.MoveTowards( transform.position, _targetLocation, _fleeingSpeed / 100 );
    }

    private void Knockback() {
        _targetLocation = new Vector3( transform.position.x, transform.localScale.y / 2, transform.position.z ) + Vector3.forward;

        transform.position = Vector3.MoveTowards( transform.position, _targetLocation, _knockbackSpeed / 100 );
    }

    private void Die() {
        float scaleDiff = ( ( transform.localScale.x + transform.localScale.y + transform.localScale.z ) / 3 ) / _deathTime;
        transform.localScale -= new Vector3( scaleDiff, scaleDiff, scaleDiff );
    }

    IEnumerator HitTimer( float pHitTime ) {
        pHitTime -= Time.deltaTime;
        _currentHitTime = pHitTime;
        Knockback();

        if ( pHitTime > 0 ) {
            yield return null;
        }

        _currentstate = State.IDLE;
    }

    IEnumerator DeathTimer ( float pDeathTime ) {
        pDeathTime -= Time.deltaTime;
        _currentDeathTime = pDeathTime;
        Die();

        if ( pDeathTime > 0 ) {
            yield return null;
        }

        Destroy( gameObject );
    }


    void OnTriggerEnter(Collider Player)
    {

        if (Player.gameObject.tag == "DeathTrigger")
        {
            GetComponent<IDamagable>().Death();
            Debug.Log("t");
        }
    }
}
