﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
class WorldGenConfig
{
    [SerializeField]
    public int amount = 1;
    public GameObject MapPartGameObject;
}


public class WorldGenerator : MonoBehaviour
{
    static WorldGenerator instance;

    [SerializeField]
    float movementSpeed;

    [SerializeField]
    List<WorldGenConfig> MapConfig = new List<WorldGenConfig>();
    [SerializeField]
    GameObject StartObject;

    [SerializeField]
    Transform Endposition;

    int WorldStartObjects = 3;


    Dictionary<string, ObjectPool> MapObjectPools = new Dictionary<string, ObjectPool>();

    List<string> objectStringList = new List<string>();
    MapPart LastMapPart;

    Vector3 LastMapEndPosistion { get { return LastMapPart == null ? transform.position : LastMapPart.EndPos; } }

    private void OnValidate()
    {
        if (null != StartObject)
        {

            if (null == StartObject.GetComponent<MapPart>())
            {
                EditorUtility.DisplayDialog("Script missing on gameobject", string.Format("StartObject is missing a '{0}' Component on the root gameObject", null == StartObject.GetComponent<MapPart>() ? "MapPart" : "NavMeshSurface"), "continue");
                StartObject = null;
            }
        }

        for (int i = 0; i < MapConfig.Count; i++)
        {
            if (null != MapConfig[i].MapPartGameObject)
            {
                if (null == MapConfig[i].MapPartGameObject.GetComponent<MapPart>())
                {
                    EditorUtility.DisplayDialog("Script missing on gameobject", string.Format(MapConfig[i].MapPartGameObject.name + " GameObject is missing a '{0}' Component on the root gameObject", null == StartObject.GetComponent<MapPart>() ? "MapPart" : "NavMeshSurface"), "continue");
                    MapConfig.RemoveAt(i);
                }
            }
        }
    }

    private void Start()
    {
        instance = this;
        GenerateData();

        SpawnStartPoint();

        for (int i = 0; i < WorldStartObjects; i++)
        {
            SetNextRandomMapPart();
        }

    }

    private GameObject SpawnObject(GameObject go)
    {
        GameObject newOBJ = Instantiate(go, LastMapEndPosistion, Quaternion.identity);
        // ActiveMapParts.Add(newOBJ.GetComponent<MapPart>());
        newOBJ.transform.SetParent(transform);

        return newOBJ;
    }

    void GenerateData()
    {
        for (int i = 0; i < MapConfig.Count; i++)
        {
            WorldGenConfig indexConfig = MapConfig[i];
            MapObjectPools.Add(indexConfig.MapPartGameObject.name, new ObjectPool(indexConfig.MapPartGameObject, 3));

            for (int k = 0; k < 2; k++)
            {
                objectStringList.Add(MapConfig[i].MapPartGameObject.name);
            }
        }
    }

    GameObject SetNextRandomMapPart()
    {
        MapPart newLastMapPart;
        do
        {
            string objString = objectStringList[Random.Range(0, objectStringList.Count)];
            newLastMapPart = MapObjectPools[objString].ActivateObjectFromPool<MapPart>(LastMapEndPosistion + Vector3.left * WorldMoveSpeed * Time.deltaTime) as MapPart;
        } while (newLastMapPart == null);

        LastMapPart = newLastMapPart;

        LastMapPart.transform.SetParent(transform);

        return LastMapPart.gameObject;
    }

    GameObject SpawnStartPoint()
    {
        LastMapPart = Instantiate(StartObject, LastMapEndPosistion + Vector3.left * WorldMoveSpeed * Time.deltaTime, Quaternion.identity).GetComponent<MapPart>();

        return LastMapPart.gameObject;
    }

    public static GameObject NextRandomMapPart()
    {
        return instance.SetNextRandomMapPart();
    }
    public static float EndPos { get { return instance.Endposition.position.x; } }
    public static float WorldMoveSpeed { get { return instance.movementSpeed; } }
}
