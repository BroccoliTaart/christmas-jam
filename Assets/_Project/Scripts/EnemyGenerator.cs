﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{

    [SerializeField]
    List<GameObject> aiMobs = new List<GameObject>();

    List<GameObject> ActiveMobs = new List<GameObject>();

    [SerializeField]
    int maxAiActive;
    [SerializeField, Range(0f, 1f)]
    float spawnChance = .5f;

    static EnemyGenerator instance;


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void OnSpawnPointActivated(Transform pLocation)
    {
        instance.UpdateMobList();

        if (instance.ActiveMobs.Count < instance.maxAiActive && Random.Range(0f, 1) < instance.spawnChance)
        {
            instance.ActiveMobs.Add(Instantiate(instance.aiMobs[Random.Range(0, instance.aiMobs.Count)], pLocation.position, Quaternion.identity));
        }
    }

    public static void OnSpawnPointActivated(Transform pLocation, float pSpawnChance)
    {
        instance.UpdateMobList();

        if (instance.ActiveMobs.Count < instance.maxAiActive && Random.Range(0f, 1) < pSpawnChance)
        {
            instance.ActiveMobs.Add(Instantiate(instance.aiMobs[Random.Range(0, instance.aiMobs.Count)], pLocation.position, Quaternion.identity));
        }
    }


    void UpdateMobList()
    {
        for (int i = 0; i < ActiveMobs.Count; i++)
        {
            if (ActiveMobs[i] == null)
            {
                ActiveMobs.RemoveAt(i);
                i--;
            }

        }
    }

}
