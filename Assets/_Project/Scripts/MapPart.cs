﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapPart : MonoBehaviour
{
    [SerializeField]
    Transform EndPosistion;

    public Vector3 EndPos { get { return EndPosistion.position; } }

    MobSpawner[] localMobSpawners;


    void DisableObject()
    {
        gameObject.SetActive(false);
       // GetComponent<NavMeshSurface>().RemoveData();
        doOnce = false;
    }

    private void Start()
    {
        localMobSpawners = transform.GetComponentsInChildren<MobSpawner>();

    }

    bool doOnce;

    private void Update()
    {
        if (!doOnce)
        {
            foreach (MobSpawner item in localMobSpawners)
            {
                item.init();
            }
            doOnce = true;
        }
        transform.position -= Vector3.right * WorldGenerator.WorldMoveSpeed * Time.deltaTime;

        if (WorldGenerator.EndPos > EndPos.x)
        {
            DisableObject();
            WorldGenerator.NextRandomMapPart();
        }
    }
}
