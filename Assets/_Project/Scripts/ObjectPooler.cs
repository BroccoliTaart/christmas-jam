﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPool
{
    int MaxPoolSize;
    GameObject poolObject;
    [SerializeField]
    List<GameObject> objectPool = new List<GameObject>();

    public ObjectPool(GameObject pObject, int pMaxPoolSize)
    {
        if (null == pObject)
        {
            Debug.Log("Trying to create a ObjcetPool with for empty GameObject");
        }
        else
        {
            poolObject = pObject;
            MaxPoolSize = pMaxPoolSize;
        }
    }

    public MonoBehaviour ActivateObjectFromPool<T>(Vector3 pPosition)
    {
        GameObject obj = FindObjectInPool() ?? CreateItemForPool();

        if (obj == null)
            return null;

        obj.transform.position = pPosition;
        obj.SetActive(true);

        if (obj.GetComponent<T>() == null)
            Debug.LogError("ComponentNotValid");

        return obj.GetComponent<T>() as MonoBehaviour;
    }

    public void DisableObject(GameObject obj)
    {
        obj.SetActive(false);
    }

    GameObject FindObjectInPool()
    {
        foreach (GameObject go in objectPool)
        {
            if (!go.activeSelf)
                return go;
        }

        return null;
    }

    GameObject CreateItemForPool()
    {
        if (objectPool.Count > MaxPoolSize)
            return null;

        GameObject gm = GameObject.Instantiate(poolObject);
        objectPool.Add(gm);

        return gm;
    }

}
