﻿using UnityEngine;

public class MobSpawner : MonoBehaviour
{
    [SerializeField]
    bool useLocalSpawnChance = false;

    [SerializeField, Range(0f, 1f)]
    float localSpawnChance = 1f;


    //TODO move onenable code to start

    public void init()
    {
        if (useLocalSpawnChance)
        {
            EnemyGenerator.OnSpawnPointActivated(transform, localSpawnChance);
        }
        else
        {
            EnemyGenerator.OnSpawnPointActivated(transform);
        }
    }
}

