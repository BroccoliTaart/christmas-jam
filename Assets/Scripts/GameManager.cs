﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public int alivePlayers;

    [HideInInspector]
    public UnityEvent PlayerCheck = new UnityEvent();

    void Start()
    {
        if (instance == null)
            instance = this;

        CheckPlayersAlive();

        PlayerCheck.AddListener(CheckPlayersAlive);
    }

    void CheckPlayersAlive()
    {
        alivePlayers = Object.FindObjectsOfType<PlayerController>().Length;

        if (alivePlayers == 1)
            EndGame();

        if (alivePlayers == 0)
            Debug.Log("Everyone is dead!");
    }

    void EndGame()
    {
        Debug.Log("Endgame!");
    }
}
