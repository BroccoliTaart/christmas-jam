﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ÇontrollerPanelHandler : MonoBehaviour
{
    public Text controllerType;
    public Text controllerNumber;
    public Image color;
    public Image button;
    public Text readyText;

    public bool isReady = false;

    List<Color> colors = new List<Color>() { Color.red, Color.green, Color.blue, Color.yellow };

    public void Setup(string type)
    {
        for (int i = 0; i < (transform.parent.childCount <= 4 ? transform.parent.childCount : 4); i++)
        {
            if (transform.parent.GetChild(i) == this.transform)
            {
                color.color = colors[i];
                controllerNumber.text = (i + 1).ToString();
                button.color = colors[i];
                readyText.text = isReady ? "Ready!" : "Not Ready";
                break;
            }
        }
        controllerType.text = type;

    }

    public void SetReady()
    {
        isReady = !isReady;
    }

    void Update()
    {
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            if (transform.parent.GetChild(i) == this.transform)
            {
                color.color = colors[i];
                controllerNumber.text = (i + 1).ToString();
                button.color = colors[i];
                readyText.text = isReady ? "Ready!" : "Not Ready";
                break;
            }
        }
    }
}
