﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectDisconnectButton : MonoBehaviour
{

    public int GetNum()
    {
        int temp = 0;
        for (int i = 0; i < transform.parent.parent.childCount; i++)
        {
            if (transform.parent.parent.GetChild(i) == this.transform.parent)
            {
                temp = i;
            }
        }
        return temp;
    }

    public void Disconect()
    {
        ControllerData obj = FindObjectOfType<ControllerData>();
        for (int i = 0; i < transform.parent.parent.childCount; i++)
        {
            if (transform.parent.parent.GetChild(i) == this.transform.parent)
            {
                obj.Disconnect(i);
                break;
            }
        }
        Debug.Log("Disconect");
    }
}
