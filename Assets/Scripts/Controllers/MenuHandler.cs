﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum MenuState
{
    ControllerConnect,
    StartScreen
}


public class MenuHandler : MonoBehaviour
{
    public MenuState menuState = MenuState.ControllerConnect;

    public GameObject controllerPanels;
    public GameObject startButton;
    public Transform spawnToHere;
    public Transform pointerPanel;

    public Text playernum;

    public List<GameObject> panels = new List<GameObject>();

    void Start()
    {
        if(GameStateManager.CurrentGameState == GameState.StartUpState)
        {
            DontDestroyOnLoad(this.gameObject);
        }
        SetActive(0);
    }

    public void SetActive(int i)
    {
        for(int f = 0; f < panels.Count; f++)
        {
            if(f == i)
            {
                panels[f].SetActive(true);
            }else
            {
                panels[f].SetActive(false);
            }
        }
    }

    void Update()
    {
        if(menuState == MenuState.ControllerConnect)
        {
            if(controllerPanels.transform.childCount > 1)
            {
                bool everyoneIsReady = true;
                for(int i = 0; i < controllerPanels.transform.childCount; i++)
                {
                    if (!controllerPanels.transform.GetChild(i).gameObject.GetComponent<ÇontrollerPanelHandler>().isReady)
                    {
                        everyoneIsReady = false;
                    }
                }
                if (everyoneIsReady)
                {
                    startButton.SetActive(true);
                }else
                {
                    startButton.SetActive(false);
                }
            }else
            {
                startButton.SetActive(false);
            }
        }
    }
}
