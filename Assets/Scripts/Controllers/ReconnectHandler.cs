﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReconnectHandler : MonoBehaviour
{

    ControllerData data;
    ConnectControllers connect;

    int playercount = 0;

    int counter = 0;

    public GameObject controllerPanel;
    public Transform spawnToHere;

    public bool checkForReady = false;

    void Start()
    {
        data = FindObjectOfType<ControllerData>();
        connect = FindObjectOfType<ConnectControllers>();
    }

    void Update()
    {
        if (checkForReady)
        {
            bool allREady = true;
            for(int i = 0; i < spawnToHere.childCount; i++)
            {
                if (!spawnToHere.GetChild(i).GetComponent<ÇontrollerPanelHandler>().isReady)
                {
                    allREady = false;
                }
            }
            if (allREady)
            {
                GameStateManager.CurrentGameState = GameState.InGameState;
                GameStateManager.CurrentControllerState = ControllerState.QueueConnectRequestsState;
                connect.connected.RemoveAllListeners();
                ClearVieuw();
                FindObjectOfType<MenuHandler>().SetActive(2);
                allREady = false;
            }
        }
    }

    public void Reconnect(int playerCount)
    {
        Debug.Log("Reconnect start");
        checkForReady = false;
        counter = 0;
        playercount = playerCount;
        ClearVieuw();
        connect.connected.AddListener(NewController);
        FindObjectOfType<MenuHandler>().playernum.text = "Player " + (spawnToHere.childCount + 1) + '\n' + "Please press L + R";
    }

    public void NewController()
    {
        ClearVieuw();
        FindObjectOfType<MenuHandler>().playernum.text = "Player " + (spawnToHere.childCount + 1) + '\n' + "Please press L + R";
        if (spawnToHere.childCount < playercount)
        {
            for (int i = 0; i < (data.currentActiveControllers.Count <= 4 ? data.currentActiveControllers.Count : 4); i++)
            {
                InputReference.controllerInputs[i].InputController = data.currentActiveControllers[i];

                GameObject obj = Instantiate(controllerPanel, spawnToHere);
                obj.GetComponent<ÇontrollerPanelHandler>().Setup(data.currentActiveControllers[i].controllers[0].isJoycon ? data.currentActiveControllers[i].controllers.Count == 1 ? "Single Joycon Device" : "Dual Joycon Device" : data.currentActiveControllers[i].controllers[0].device.Name);
                obj.transform.localScale = new Vector3(1, 1, 1);

                FindObjectOfType<ConnectControllers>();
            }

        }
        if(spawnToHere.childCount >= playercount)
        {
            FindObjectOfType<MenuHandler>().playernum.text = "both players please set yourself ready";
            checkForReady = true;
        }
    }

    void ClearVieuw()
    {
        for (int i = 0; i < spawnToHere.childCount; i++)
        {
            Destroy(spawnToHere.GetChild(i).gameObject);
        }
        InputReference.ClearInputList();
    }
}
