﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerData : MonoBehaviour
{
    [SerializeField]
    TestForControllers controllers;

    [SerializeField]
    int lastControllerCount = -1;

    //[HideInInspector]
    public List<Controller> controllerList = new List<Controller>();
    public List<Controller> changedControllerList = new List<Controller>();

    public List<ControllerFinalized> currentActiveControllers = new List<ControllerFinalized>();
    public List<ControllerFinalized> lastActiveControllerList = new List<ControllerFinalized>();

    public List<InputEventHandler> inputEventHandlers = new List<InputEventHandler>();

    public List<Controller> blacklist = new List<Controller>();

    public UnityEvent repopulate = new UnityEvent();

    void Start()
    {
        if(GameStateManager.CurrentGameState == GameState.StartUpState)
        {
            DontDestroyOnLoad(this.gameObject);
        }
        controllers = FindObjectOfType<TestForControllers>();
        inputEventHandlers = InputReference.controllerInputs;
    }

    void Update()
    {
        CreateControllers();
        CheckForControllerChanges();
    }

    public void Disconnect(int i)
    {
        for (int f = 0; f < currentActiveControllers[i].controllers.Count; f++)
        {
            if (blacklist.Contains(currentActiveControllers[i].controllers[f]))
            {
                blacklist.Remove(currentActiveControllers[i].controllers[f]);
            }
        }
        currentActiveControllers.RemoveAt(i);
        CheckForControllerChanges();
    }

    void CheckForControllerChanges()
    {
        for (int i = 0; i < currentActiveControllers.Count; i++)
        {
            if (!lastActiveControllerList.Contains(currentActiveControllers[i]))
            {
                lastActiveControllerList.Clear();
                for (int f = 0; f < currentActiveControllers.Count; f++)
                {
                    lastActiveControllerList.Add(currentActiveControllers[f]);
                }
                repopulate.Invoke();
                break;
            }
        }
        for (int i = 0; i < lastActiveControllerList.Count; i++)
        {
            if (!currentActiveControllers.Contains(lastActiveControllerList[i]))
            {

                lastActiveControllerList.Clear();
                for (int f = 0; f < currentActiveControllers.Count; f++)
                {
                    lastActiveControllerList.Add(currentActiveControllers[f]);
                }
                repopulate.Invoke();
                break;
            }
        }
    }

    void CreateControllers()
    {
        if ((controllers.workingControllers.Count + controllers.joycons.Count) != lastControllerCount)
        {
            lastControllerCount = (controllers.workingControllers.Count + controllers.joycons.Count);
            changedControllerList.Clear();

            if (controllers.workingControllers.Count > 0)
            {
                for (int i = 0; i < controllers.workingControllers.Count; i++)
                {
                    changedControllerList.Add(new Controller(controllers.workingControllers[i]));
                }
            }

            if (controllers.joycons.Count > 0)
            {
                for (int i = 0; i < controllers.joycons.Count; i++)
                {
                    changedControllerList.Add(new Controller(controllers.joycons[i]));
                }
            }

            controllerList = changedControllerList;
            currentActiveControllers.Clear();
            blacklist.Clear();
            for (int i = 0; i < controllerList.Count; i++)
            {
                if (controllerList[i].isJoycon)
                {
                    controllerList[i].joycon.SetRumble(100, 100, 100, 500);
                }
            }
            repopulate.Invoke();

            return;
        }
    }
}
