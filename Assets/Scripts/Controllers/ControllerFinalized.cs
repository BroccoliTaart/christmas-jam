﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControllerFinalized
{
    public List<Controller> controllers = new List<Controller>();

    public ControllerFinalized()
    {

    }

    public ControllerFinalized(List<Controller> contr)
    {
        controllers = contr;
    }

    public void Setup(List<Controller> contr)
    {
        controllers = contr;
    }
}
