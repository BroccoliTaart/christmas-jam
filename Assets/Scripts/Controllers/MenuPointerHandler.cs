﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPointerHandler : MonoBehaviour
{
    List<Color> colors = new List<Color>() { Color.red, Color.green, Color.blue, Color.yellow };
    List<GameObject> pointers = new List<GameObject>();
    public GameObject pointer;
    public Transform pointerPanel;

    void OnDisable()
    {
        foreach(GameObject point in pointers)
        {
            try
            {
                point.SetActive(false);
            }
            catch { }
        }
    }

    void Start()
    {
        pointerPanel = FindObjectOfType<MenuHandler>().pointerPanel;
        for(int i = 0; i < colors.Count; i++)
        {
            GameObject obj = Instantiate(pointer, pointerPanel);
            obj.transform.localPosition = new Vector3(0, 0, 0);
            obj.GetComponent<Image>().color = colors[i];
            pointers.Add(obj);
        }
    }

    void Update()
    {
        for(int i = 0; i < pointers.Count; i++)
        {
            if(i < InputReference.GetCount())
            {
                pointers[i].SetActive(true);
            }else
            {
                pointers[i].SetActive(false);
            }
        }
    }
}
