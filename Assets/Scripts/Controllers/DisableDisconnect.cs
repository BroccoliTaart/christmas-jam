﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableDisconnect : MonoBehaviour
{

    void Update()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<ÇontrollerPanelHandler>().button.gameObject.SetActive(false);
        }
    }
}
