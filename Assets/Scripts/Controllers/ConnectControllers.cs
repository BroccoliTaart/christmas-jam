﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

//[RequireComponent(typeof(TestForControllers))]
public class ConnectControllers : MonoBehaviour
{
    [SerializeField]
    TestForControllers controllers;
    [SerializeField]
    ControllerData controllerData;

    public UnityEvent connected = new UnityEvent();


    void Start()
    {
        controllerData = FindObjectOfType<ControllerData>();
        controllerData.controllerList.Clear();
        controllers = FindObjectOfType<TestForControllers>();
        for (int i = 0; i < FindObjectsOfType<InputEventHandler>().Length; i++)
        {
            InputReference.controllerInputs.Add(FindObjectsOfType<InputEventHandler>()[i]);
        }
        controllerData.inputEventHandlers = InputReference.controllerInputs;
    }

    void Update()
    {
        if (GameStateManager.CurrentControllerState == ControllerState.FreeConnectAndDisconnectState || GameStateManager.CurrentControllerState == ControllerState.ReconnectControllersState)
        {
            DetectInputs();
        }
    }

    public void DetectInputs()
    {
        Controller firstPressed = null;
        ControllerFinalized finalizedController = null;
        bool breakme = false;
        for (int f = 0; f < 2; f++)
        {
            if (breakme)
            {
                break;
            }
            for (int i = 0; i < controllerData.controllerList.Count; i++)
            {
                if (!controllerData.blacklist.Contains(controllerData.controllerList[i]))
                {
                    if (controllerData.controllerList[i].isJoycon)
                    {
                        if (controllerData.controllerList[i].joycon.GetButton(Joycon.Button.SL) && controllerData.controllerList[i].joycon.GetButton(Joycon.Button.SR))
                        {
                            controllerData.blacklist.Add(controllerData.controllerList[i]);
                            controllerData.controllerList[i].joycon.SetRumble(100, 100, 100, 500);
                            finalizedController = new ControllerFinalized(new List<Controller>() { controllerData.controllerList[i] });
                            breakme = true;
                            break;
                        }
                        if (controllerData.controllerList[i].joycon.GetButton(Joycon.Button.SHOULDER_1) && controllerData.controllerList[i].joycon.isLeft)
                        {
                            firstPressed = controllerData.controllerList[i];
                            //Debug.Log("left trigger pressed");
                        }
                        if (controllerData.controllerList[i].joycon.GetButton(Joycon.Button.SHOULDER_1) && !controllerData.controllerList[i].joycon.isLeft)
                        {
                            //Debug.Log("Right Trigger Pressed");
                            if (firstPressed != null)
                            {
                                finalizedController = new ControllerFinalized(new List<Controller>() { firstPressed, controllerData.controllerList[i] });
                                controllerData.blacklist.Add(firstPressed);
                                controllerData.blacklist.Add(controllerData.controllerList[i]);
                                breakme = true;
                                firstPressed.joycon.SetRumble(100, 100, 100, 500);
                                controllerData.controllerList[i].joycon.SetRumble(100, 100, 100, 500);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (controllerData.controllerList[i].device.LeftBumper.IsPressed && controllerData.controllerList[i].device.RightBumper.IsPressed)
                        {
                            finalizedController = new ControllerFinalized(new List<Controller>() { controllerData.controllerList[i] });
                            controllerData.blacklist.Add(controllerData.controllerList[i]);
                            breakme = true;
                            break;
                        }
                    }
                }
            }
        }
        if (finalizedController != null && !controllerData.currentActiveControllers.Contains(finalizedController))
        {
            controllerData.currentActiveControllers.Add(finalizedController);
            connected.Invoke();
            return;
        }
    }
}
