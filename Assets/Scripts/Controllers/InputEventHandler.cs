﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using InControl;

[System.Serializable]
public class MyUnityEvent<T> : UnityEvent<T> { }

[System.Serializable]
public class MyUnityEvent<T, TT> : UnityEvent<T, TT> { }

[System.Serializable]
public class InputEventHandler : MonoBehaviour
{
    [SerializeField]
    ControllerFinalized inputController = new ControllerFinalized();

    [HideInInspector]
    public MyUnityEvent<float, float> linkerStickAs = new MyUnityEvent<float, float>();

    [HideInInspector]
    public MyUnityEvent<float, float> rechterStickAs = new MyUnityEvent<float, float>();

    [HideInInspector]
    public MyUnityEvent<float, float> arrowKeysAs = new MyUnityEvent<float, float>();

    [HideInInspector]
    public UnityEvent aButtonDown = new UnityEvent();
    [HideInInspector]
    public UnityEvent aButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent aButtonUp = new UnityEvent();
    [HideInInspector]
    public UnityEvent bButtonDown = new UnityEvent();
    [HideInInspector]
    public UnityEvent bButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent bButtonUp = new UnityEvent();
    [HideInInspector]
    public UnityEvent yButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent xButtonDown = new UnityEvent();
    [HideInInspector]
    public UnityEvent xButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent xButtonUp = new UnityEvent();

    [HideInInspector]
    public UnityEvent lButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent rButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent zlButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent zrButtonPress = new UnityEvent();

    [HideInInspector]
    public UnityEvent plusButtonPress = new UnityEvent();
    [HideInInspector]
    public UnityEvent minusButtonPress = new UnityEvent();

    [HideInInspector]
    public UnityEvent homeButtonPress = new UnityEvent();

    public ControllerFinalized InputController
    {
        get { return inputController; }
        set { inputController = value; }
    }

    bool abutt = false;
    bool xbutt = false;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        if (inputController.controllers.Count > 0)
        {
            for (int i = 0; i < inputController.controllers.Count; i++)
            {
                Controller curContr = inputController.controllers[i];
                if (curContr.isJoycon)
                {
                    Joycon curJoy = curContr.joycon;
                    if (inputController.controllers.Count > 1)
                    {
                        //dual joycon setup
                        if (!curJoy.isLeft)
                        {
                            rechterStickAs.Invoke(curJoy.GetStick()[0], curJoy.GetStick()[1]);
                            if (curJoy.GetButtonDown(Joycon.Button.DPAD_RIGHT))
                            {
                                aButtonDown.Invoke();
                            }
                            else if (curJoy.GetButton(Joycon.Button.DPAD_RIGHT))
                            {
                                aButtonPress.Invoke();
                            }
                            else if (curJoy.GetButtonUp(Joycon.Button.DPAD_RIGHT))
                            {
                                aButtonUp.Invoke();
                            }
                        }
                        else
                        {
                            linkerStickAs.Invoke(curJoy.GetStick()[0], curJoy.GetStick()[1]);
                        }
                    }else
                    {
                        //single joycon setup
                        if (curJoy.isLeft)
                        {
                            linkerStickAs.Invoke(-curJoy.GetStick()[1], curJoy.GetStick()[0]);
                            if (curJoy.GetButtonDown(Joycon.Button.DPAD_DOWN))
                            {
                                aButtonDown.Invoke();
                            }
                            else if (curJoy.GetButton(Joycon.Button.DPAD_DOWN))
                            {
                                aButtonPress.Invoke();
                            }
                            else if (curJoy.GetButtonUp(Joycon.Button.DPAD_DOWN))
                            {
                                aButtonUp.Invoke();
                            }
                        }
                        else
                        {
                            linkerStickAs.Invoke(curJoy.GetStick()[1], -curJoy.GetStick()[0]);
                            if (curJoy.GetButtonDown(Joycon.Button.DPAD_UP))
                            {
                                aButtonDown.Invoke();
                            }
                            else if (curJoy.GetButton(Joycon.Button.DPAD_UP))
                            {
                                aButtonPress.Invoke();
                            }
                            else if (curJoy.GetButtonUp(Joycon.Button.DPAD_UP))
                            {
                                aButtonUp.Invoke();
                            }
                        }
                       
                    }   
                }
                else
                {
                    InputDevice indev = curContr.device;
                    linkerStickAs.Invoke(indev.LeftStickX.Value, indev.LeftStickY.Value);
                    rechterStickAs.Invoke(indev.RightStickX.Value, indev.RightStickY.Value);

                    if (indev.Action2.IsPressed)
                    {
                        if (!abutt)
                        {
                            aButtonDown.Invoke();
                            abutt = true;
                        }
                        else
                        {
                            aButtonPress.Invoke();
                        }
                    }
                    else
                    {
                        if (abutt)
                        {
                            aButtonUp.Invoke();
                            abutt = false;
                        }
                    }

                    if (indev.Action4.IsPressed)
                    {
                        if (!xbutt)
                        {
                            xButtonDown.Invoke();
                            xbutt = true;
                        }
                        else
                        {
                            xButtonPress.Invoke();
                        }
                    }
                    else
                    {
                        if (xbutt)
                        {
                            xButtonUp.Invoke();
                            xbutt = false;
                        }
                    }

                }
            }
        }
    }
}

