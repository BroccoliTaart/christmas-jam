﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerMovement : MonoBehaviour
{
    [SerializeField]
    InputEventHandler inputEvent;

    int playerNum()
    {
        int temp = 0;
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            if (transform.parent.GetChild(i) == this.transform)
            {
                temp = i;
            }
        }
        return temp;
    }

    void Start()
    {
        for(int i= 0; i < transform.parent.childCount; i++)
        {
            if(transform.parent.GetChild(i) == this.transform)
            {
                inputEvent = InputReference.controllerInputs[i];
                inputEvent.linkerStickAs.AddListener(MovementLinkerStick);
                inputEvent.rechterStickAs.AddListener(MovementLinkerStick);
                inputEvent.aButtonDown.AddListener(AButtonDown);
                inputEvent.aButtonUp.AddListener(AButtonUp);
            }
        }
    }

    void Update()
    {
        if (!InputReference.controllerInputs.Contains(inputEvent))
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (transform.parent.GetChild(0) == this.transform)
                {
                    inputEvent = InputReference.controllerInputs[i];
                    inputEvent.linkerStickAs.AddListener(MovementLinkerStick);
                    inputEvent.rechterStickAs.AddListener(MovementLinkerStick);
                    inputEvent.aButtonDown.AddListener(AButtonDown);
                    inputEvent.aButtonUp.AddListener(AButtonUp);
                }
            }
        }
    }
    bool trigger = false;

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.GetComponent<DetectDisconnectButton>() != null) {
            if (col.GetComponent<Button>() != null && col.GetComponent<DetectDisconnectButton>().GetNum() == playerNum())
            {
                if (trigger)
                {
                    col.GetComponent<Button>().onClick.Invoke();
                    trigger = false;
                }
            }
        }

        if (col.GetComponent<Button>())
        {
            if (trigger)
            {
                col.GetComponent<Button>().onClick.Invoke();
                trigger = false;
            }
        }
    }

    void AButtonDown()
    {
        trigger = true;
        transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
    }

    void AButtonUp()
    {
        trigger = false;
        transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    void MovementLinkerStick(float x, float y)
    {
        transform.position += new Vector3(x * 1000 * Time.deltaTime, y * 1000 * Time.deltaTime, 0);
        if(transform.position.x < 0)
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }

        if (transform.position.x > 1920)
        {
            transform.position = new Vector3(1920, transform.position.y, transform.position.z);
        }

        if (transform.position.y < 0)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }

        if (transform.position.y > 1080)
        {
            transform.position = new Vector3(transform.position.x, 1080, transform.position.z);
        }
    }
}
