﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public bool loadOnStart = false;
    public string loadScene = "";

    void Start()
    {
        if (loadOnStart)
        {
            SceneManager.LoadScene(loadScene);
        }
    }

    public void SwitchScene(string input)
    {
        SceneManager.LoadScene(input);
    }
}
