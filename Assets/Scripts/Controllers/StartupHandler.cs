﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartupHandler : MonoBehaviour
{
    void Start()
    {
        if (GameStateManager.CurrentGameState == GameState.StartUpState)
        {
            GameStateManager.CurrentGameState = GameState.MenuState;
            GameStateManager.CurrentControllerState = ControllerState.FreeConnectAndDisconnectState;
        }
    }
}
