﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameButton : MonoBehaviour
{

    public MenuHandler handler;

    public void StartGame()
    {
        handler = FindObjectOfType<MenuHandler>();
        handler.menuState = MenuState.StartScreen;
        GameStateManager.CurrentGameState = GameState.InGameState;
        GameStateManager.CurrentControllerState = ControllerState.QueueConnectRequestsState;
        GameStateManager.playerCount = handler.spawnToHere.childCount;
        Debug.Log(GameStateManager.playerCount);
        handler.SetActive(2);
    }

    void Update()
    {
        
    }
}
