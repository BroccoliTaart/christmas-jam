﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Controller
{
    public bool isJoycon = false;
    public Joycon joycon;
    public InputDevice device;

    public Controller(Joycon con)
    {
        isJoycon = true;
        joycon = con;
    }

    public Controller(InputDevice device)
    {
        isJoycon = false;
        this.device = device;
    } 
}
