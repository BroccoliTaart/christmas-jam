﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControllerPanel : MonoBehaviour
{
    public Transform spawnToHere;
    ControllerData controllerData;
    public GameObject controllerElement;



    void Start()
    {
        controllerData = FindObjectOfType<ControllerData>();
        controllerData.repopulate.AddListener(Repopulate);
        spawnToHere = FindObjectOfType<MenuHandler>().spawnToHere;
    }

    public void Repopulate()
    {
        for (int i = 0; i < spawnToHere.childCount; i++)
        {
            Destroy(spawnToHere.GetChild(i).gameObject);
        }
        InputReference.ClearInputList();
        for (int i = 0; i < (controllerData.currentActiveControllers.Count <= 4 ? controllerData.currentActiveControllers.Count : 4); i++)
        {
            InputReference.controllerInputs[i].InputController = controllerData.currentActiveControllers[i];

            GameObject obj = Instantiate(controllerElement, spawnToHere);
            obj.GetComponent<ÇontrollerPanelHandler>().Setup(controllerData.currentActiveControllers[i].controllers[0].isJoycon ? controllerData.currentActiveControllers[i].controllers.Count == 1 ? "Single Joycon Device" : "Dual Joycon Device" : controllerData.currentActiveControllers[i].controllers[0].device.Name);
            obj.transform.localScale = new Vector3(1, 1, 1);

            FindObjectOfType<ConnectControllers>();
        }
    }
}
