﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    StartUpState,
    MenuState,
    InGameState,
    InPauseState
}

public enum ControllerState
{
    FreeConnectAndDisconnectState,
    QueueConnectRequestsState,
    ReconnectControllersState
}


public static class GameStateManager
{
    static GameState currentGameState = GameState.StartUpState;
    static GameState lastGameState = GameState.StartUpState;

    public static int playerCount = 0;

    static ControllerState currentControllerState = ControllerState.FreeConnectAndDisconnectState;
    static ControllerState lastControllerState = ControllerState.FreeConnectAndDisconnectState;

    public static GameState CurrentGameState
    {
        get { return currentGameState;  }
        set { currentGameState = value; lastGameState = currentGameState; }
    }

    public static GameState LastGameState
    {
        get { return lastGameState; }
    }

    public static ControllerState CurrentControllerState
    {
        get { return currentControllerState; }
        set { currentControllerState = value; lastControllerState = currentControllerState; }
    }

    public static ControllerState LastControllerState
    {
        get { return lastControllerState; }
    }
}
