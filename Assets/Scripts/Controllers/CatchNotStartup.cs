﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CatchNotStartup : MonoBehaviour
{
    void Awake()
    {
        if(GameStateManager.CurrentGameState == GameState.StartUpState)
        {
            SceneManager.LoadScene("Startup");
        }
    }
}
