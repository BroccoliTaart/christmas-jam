﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputReference
{
    public static List<InputEventHandler> controllerInputs = new List<InputEventHandler>();

    public static void ClearInputList()
    {
        for(int i = 0; i < controllerInputs.Count; i++)
        {
            controllerInputs[i].InputController = new ControllerFinalized();
        }
    }

    public static void FlipList()
    {
        controllerInputs.Reverse();
    }

    public static int GetCount()
    {
        int temp = 0;
        for(int i = 0; i < controllerInputs.Count; i++)
        {
            if(controllerInputs[i].InputController.controllers.Count > 0)
            {
                temp++;
            }else
            {
                return temp;
            }
        }
        return temp;
    }

}
