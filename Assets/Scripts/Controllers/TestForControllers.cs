﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class TestForControllers : MonoBehaviour
{
    [SerializeField]
    public List<InputDevice> workingControllers = new List<InputDevice>();
    [SerializeField]
    public List<Joycon> joycons = new List<Joycon>();

    List<InputDevice> queuedControllers = new List<InputDevice>();

    // Use this for initialization
    void Start()
    {
        if (GameStateManager.CurrentGameState == GameState.StartUpState)
        {
            for (int i = 0; i < InputManager.Devices.Count; i++)
            {
                if (InputManager.Devices[i].IsKnown)
                {
                    Debug.Log("attached : " + InputManager.Devices[i].Name);
                    workingControllers.Add(InputManager.Devices[i]);
                }
            }
            InputManager.OnDeviceAttached += inputDevice => OnAttach(inputDevice);
            InputManager.OnDeviceDetached += inputDevice => OnDetach(inputDevice);

            joycons = JoyconManager.Instance.j;
        }

    }

    void OnAttach(InputDevice device)
    {
        if (GameStateManager.CurrentControllerState == ControllerState.FreeConnectAndDisconnectState || GameStateManager.CurrentControllerState == ControllerState.ReconnectControllersState)
        {
            if (device.IsKnown)
            {
                Debug.Log("attached : " + device.Name);
                workingControllers.Add(device);
            }
            joycons = JoyconManager.Instance.j;
            
        }
        else if (GameStateManager.CurrentControllerState == ControllerState.QueueConnectRequestsState )
        {
            if (device.IsKnown)
            {
                queuedControllers.Add(device);
            }
        }
    }

    void OnDetach(InputDevice device)
    {
        Debug.Log("detached : " + device.Name);
        if (device.IsKnown)
        {
            workingControllers.Remove(device);
        }
        else
        {
            joycons = JoyconManager.Instance.j;
        }

        if(GameStateManager.CurrentGameState == GameState.InGameState)
        {
            GameStateManager.CurrentGameState = GameState.InPauseState;
            GameStateManager.CurrentControllerState = ControllerState.ReconnectControllersState;
            ReconnectControllerStateSetup();
        }
    }

    void ReconnectControllerStateSetup()
    {
        Debug.Log("Should reconnect");
        for(int i = 0; i < queuedControllers.Count; i++)
        {
            workingControllers.Add(queuedControllers[i]);
        }
        queuedControllers.Clear();
        FindObjectOfType<MenuHandler>().SetActive(1);
        FindObjectOfType<ReconnectHandler>().Reconnect(GameStateManager.playerCount);
    }

    void Update()
    {

    }

    void OnGUI()
    {
        string allDevices = "";
        for (int i = 0; i < workingControllers.Count; i++)
        {
            allDevices += workingControllers[i].Name + " : ";
        }

        for (int i = 0; i < joycons.Count; i++)
        {
            allDevices += (joycons[i].isLeft ? "Joycon Left" : "Joycon Right") + " : ";
        }

        //GUI.Button(new Rect(0, 30, 1000, 200), new GUIContent(allDevices));
    }
}
