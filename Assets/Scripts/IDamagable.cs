﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IDamagable : MonoBehaviour
{
    public int health;
    [SerializeField ]private Image healthbar;

    public void TakeDamage(int pDamage)
    {
        if (pDamage > 0)
        {
            health -= pDamage;

            if (health <= 0)
                Death();

            if ( null != GetComponent<RangedDemonAI>() )
                GetComponent<RangedDemonAI>().Hit();
        }
        else
            Debug.LogWarning("damage is 0 or less");
    }

    public void Death()
    {
        if (null != GetComponent<PlayerController>()) {
            Destroy(gameObject);
            SceneManager.LoadScene(0);
            Debug.Log("jew");
        }
        else if ( null != GetComponent<RangedDemonAI>() )
            GetComponent<RangedDemonAI>().Death();

    }

    private void OnGUI()
    {
        if (healthbar != null)
            healthbar.fillAmount = (float )health / 100;
    }
}
