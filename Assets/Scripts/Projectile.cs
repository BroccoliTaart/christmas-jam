﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField, Range( 0, 20 )]
    private float _expirationTime = 12;

    private float _speed;
    private int _damage;


    public float Speed {
        get {
            return _speed;
        }
        set {
            _speed = value;
        }
    }

    public int Damage {
        get {
            return _damage;
        }
        set {
            _damage = value;
        }
    }


    private void Start()
    {
        Destroy( gameObject, _expirationTime );
    }

    private void Update()
    {
        Vector3 targetLocation = transform.position + transform.forward * _speed;

        transform.position = Vector3.MoveTowards( transform.position, targetLocation, _speed / 10 );
    }

    private void OnCollisionEnter(Collision collision) {
        if ( null != collision.gameObject.GetComponent<PlayerController>() && null != collision.gameObject.GetComponent<IDamagable>() ) {
            collision.gameObject.GetComponent<IDamagable>().TakeDamage( _damage );
            Destroy( gameObject );
        }
    }
}
