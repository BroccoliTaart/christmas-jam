﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float backwardsSpeed;

    [SerializeField] CombatBase cBase;
    public Transform pivot;

    private CharacterController cControll;

    public Animator anim;

    private float attackTimer;

    [SerializeField ]private KeyCode attackKey;
    [SerializeField] private string InputForward, InputRight;

    void Start()
    {
        cControll = GetComponent<CharacterController>();
    }

    // Temp fix
    void Update()
    {

        if (Input.GetAxis(InputRight) == 0 && Input.GetAxis(InputForward) == 0)
            cControll.SimpleMove(new Vector3(-backwardsSpeed, 0,0));

        var mForward = Input.GetAxis(InputForward) * transform.TransformDirection(Vector3.forward);
        var mRight = Input.GetAxis(InputRight) * transform.TransformDirection(Vector3.right);

        Vector3 movement = Vector3.Normalize(mRight + mForward);

        float angle = Mathf.Atan2(movement.x, movement.z) * Mathf.Rad2Deg;

        if (Input.GetAxis(InputRight) != 0 || Input.GetAxis(InputForward) != 0)
            pivot.transform.rotation = Quaternion.Euler(new Vector3(0, angle, 0));

        cControll.SimpleMove(movement * speed);

        if (Input.GetKeyDown(attackKey))
            OnAttack();
    }

    void OnAttack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            cBase.Attack(this.gameObject, pivot);
    }

    void OnTriggerEnter(Collider Player)
    {

        if (Player.gameObject.tag == "DeathTrigger")
        {
            GetComponent<IDamagable>().Death();
            Debug.Log("t");
        }
    }
}
