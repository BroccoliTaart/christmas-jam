﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CombatBase : ScriptableObject
{
    public abstract void Attack(GameObject pAttacker);
    public abstract void Attack(GameObject pAttacker, Transform pTransform);
}
