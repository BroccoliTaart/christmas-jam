﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CombatBase", menuName = "New Combat Base")]
public class MeleeCombat : CombatBase
{
    public int damage;
    public float range;

    public override void Attack(GameObject pAttacker)
    {
        RaycastHit hit;
        Physics.Raycast(pAttacker.transform.position, pAttacker.transform.TransformDirection(Vector3.forward),out hit, range);

        Debug.Log("attack type: " + this + " | collider: " + hit.collider);
    }

    public override void Attack(GameObject pAttacker, Transform pTransform)
    {
        RaycastHit hit;
        Physics.Raycast(pAttacker.transform.position, pTransform.TransformDirection(Vector3.forward), out hit, range);


        if (pAttacker.GetComponent<PlayerController>() != null)
            pAttacker.GetComponent<PlayerController>().anim.SetTrigger("attack");

        if (hit.collider != null)
        {
            if ( null != hit.collider.GetComponent<IDamagable>() )
                hit.collider.GetComponent<IDamagable>().TakeDamage(damage);
        }

        Debug.Log("attack type: " + this + " | collider: " + hit.collider);

    }
}
