﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(WorldGenConfig))]
public class WorldConfigDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), new GUIContent(property.FindPropertyRelative("MapPartGameObject").objectReferenceValue == null ? "None" : property.FindPropertyRelative("MapPartGameObject").objectReferenceValue.name));

        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 1;

        // Calculate rects
        var GameObjectRect = new Rect(position.x, position.y, 200, position.height);
        var unitRect = new Rect(position.x + GameObjectRect.width + 5, position.y, 50, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(GameObjectRect, property.FindPropertyRelative("MapPartGameObject"), GUIContent.none);

        if (property.FindPropertyRelative("MapPartGameObject").objectReferenceValue != null)
        {
            EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("amount"), GUIContent.none);
        }

        // EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
